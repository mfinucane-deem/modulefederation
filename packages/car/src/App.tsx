import React, { useEffect } from "react";

const App = (): React.ReactNode => {
  useEffect((): void => {
    console.log("car App useEffect called");
  }, []);

  return (
    <div>
      <h1>Car: App</h1>
    </div>
  );
};

export default App;
