import React, { useEffect } from "react";

const App = () => {
  useEffect((): void => {
    console.log("useEffect for Train app called");
  }, []);

  return <h1>Train App is here</h1>;
};

export default App;
