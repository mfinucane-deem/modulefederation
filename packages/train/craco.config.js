const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

const deps = require("./package.json").dependencies;

module.exports = {
  mode: "development",
  devServer: {
    port: 9004,
  },
  webpack: {
    configure: {
      output: {
        publicPath: "http://localhost:9004/",
      },
    },
    plugins: {
      add: [
        new ModuleFederationPlugin({
          name: "train",
          filename: "train-app.js",
          remotes: {},
          exposes: {
            "./TrainApp": "./src/App.tsx",
          },
          shared: {
            ...deps,
            react: {
              singleton: true,
              requiredVersion: deps.react,
            },
            "react-dom": {
              singleton: true,
              requiredVersion: deps["react-dom"],
            },
          },
        }),
      ],
    },
  },
};
