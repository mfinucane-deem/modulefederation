import React, { useEffect } from "react";

const App = (): React.ReactNode => {
  useEffect((): void => {
    console.log("flight App useEffect called");
  }, []);

  return (
    <div>
      <h1>Flight: App</h1>
    </div>
  );
};

export default App;
