import React from "react";
import { render } from "@testing-library/react";

import App from "./App";

describe("App tests", (): void => {
  it("renders the component", (): void => {
    expect(() => render(<App />)).not.toThrow();
  });
});
