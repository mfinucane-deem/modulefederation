import type { Config } from "jest";

const config: Config = {
  testEnvironment: "jsdom",
  verbose: true,
  setupFiles: ["./jestsetup.tsx"],
};

export default config;
