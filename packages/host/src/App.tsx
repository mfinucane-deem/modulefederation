import React, { useCallback, useEffect, Suspense } from "react";
import { Link, Route, Routes } from "react-router-dom";
import { connect, Provider } from "react-redux";

import { store } from "./store";
import HotelState from "hotel/HotelState";

interface NavLink {
  title: string;
  to: string;
}

export interface Props {
  count: number;
  increment: () => void;
}

const delay = (ms: number = 1000): Promise<void> =>
  new Promise((resolve: () => void) =>
    setTimeout(() => {
      resolve();
    }, ms)
  );

const RemoteCarApp = React.lazy(
  async (): Promise<{ default: React.ComponentType<any> }> => {
    await delay();
    return import("car/CarApp");
  }
);

const RemoteFlightApp = React.lazy(
  async (): Promise<{ default: React.ComponentType<any> }> => {
    await delay();
    return import("flight/FlightApp");
  }
);

const RemoteHotelApp = React.lazy(
  async (): Promise<{ default: React.ComponentType<any> }> => {
    await delay();
    return import("hotel/HotelApp");
  }
);

const RemoteTrainApp = React.lazy(
  async (): Promise<{ default: React.ComponentType<any> }> => {
    await delay();
    return import("train/TrainApp");
  }
);

const LocalLazyComponent = React.lazy(
  async (): Promise<{ default: React.ComponentType<any> }> => {
    await delay();
    return import("./LazyComponent");
  }
);

const links: NavLink[] = [
  { title: "Home", to: "/" },
  { title: "Car", to: "/car" },
  { title: "Flight", to: "/flight" },
  { title: "Hotel", to: "/hotel" },
  { title: "Train", to: "/train" },
  { title: "Lazy", to: "/lazy" },
];

const DefaultHome = (): React.ReactNode => (
  <div>
    <h1>Home!</h1>
  </div>
);

const NotFound = (): React.ReactNode => (
  <div>
    <h1>Not found!</h1>
  </div>
);

const Loading = (): React.ReactNode => (
  <div>
    <h1>Wait</h1>
  </div>
);

const withSuspense = (children: React.ReactNode): React.ReactNode => (
  <Suspense fallback={<Loading />}>{children}</Suspense>
);

const Nav = ({ navLinks }: { navLinks: NavLink[] }): React.ReactNode => (
  <nav>
    <ul>
      {navLinks.map(
        ({ title, to }: NavLink): React.ReactNode => (
          <li key={to}>
            <Link title={title} to={to}>
              {title}
            </Link>
          </li>
        )
      )}
    </ul>
  </nav>
);

export const App = ({ count = 0, increment }: Props): React.ReactNode => {
  useEffect((): void => {
    console.log("host App useEffect called");
  }, []);

  const handleHotelBook = useCallback(increment, []);

  return (
    <div>
      <header>
        <h1>Host: App</h1>
        <span>Hotel stays booked: {count}</span>
        <button onClick={handleHotelBook}>Book a hotel!</button>
        <Nav navLinks={links} />
      </header>
      <Routes>
        <Route path="/" element={<DefaultHome />} />
        <Route path="/car/*" element={withSuspense(<RemoteCarApp />)} />
        <Route path="/flight/*" element={withSuspense(<RemoteFlightApp />)} />
        <Route
          path="/hotel/*"
          element={withSuspense(<RemoteHotelApp store={store} />)}
        />
        <Route path="/train/*" element={withSuspense(<RemoteTrainApp />)} />
        <Route path="/lazy/*" element={withSuspense(<LocalLazyComponent />)} />
        <Route path="*" element={withSuspense(<NotFound />)} />
      </Routes>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  count: HotelState.selectors.getCount(state),
});

const mapDispatchToProps = (dispatch: any) => ({
  increment: (): void => dispatch(HotelState.actions.increment()),
});

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

const AppWrapper = (): React.ReactNode => (
  <Provider store={store}>
    <ConnectedApp />
  </Provider>
);

export default AppWrapper;
