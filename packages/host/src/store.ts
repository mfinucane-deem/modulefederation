import { combineReducers, createStore, Store } from "redux";

const hostReducer = (state = {}, action: any) => {
  return state;
};

const staticReducers = {
  host: hostReducer,
};

const createReducer = (asyncReducers?: any) =>
  combineReducers({
    /**
     * Without 'staticReducers', we see the warning
     * redux.js:xxx Store does not have a valid reducer.
     * Make sure the argument passed to combineReducers
     * is an object whose values are reducers.
     */
    ...staticReducers,
    ...asyncReducers,
  });

export default function configureStore() {
  const store: Store = createStore(createReducer());

  store.asyncReducers = {};

  store.injectReducer = (key, asyncReducer) => {
    store.asyncReducers[key] = asyncReducer;
    store.replaceReducer(createReducer(store.asyncReducers));
  };

  return store;
}

export const store = configureStore();
