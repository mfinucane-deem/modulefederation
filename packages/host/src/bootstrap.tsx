import React, { StrictMode } from "react";
import { createRoot, Root } from "react-dom/client";
import { BrowserRouter } from "react-router-dom";

import AppWrapper from "./App";

const rootElement: HTMLElement | null = document.getElementById("host-app");
const root: Root = createRoot(rootElement!);

root?.render(
  <StrictMode>
    <BrowserRouter>
      <AppWrapper />
    </BrowserRouter>
  </StrictMode>
);
