import React, { useEffect } from "react";

const LazyComponent = (): React.ReactNode => {
  useEffect((): void => {
    console.log("LazyComponent useEffect called");
  }, []);

  return (
    <div>
      <h1>I am a lazy component</h1>
    </div>
  );
};

export default LazyComponent;
