import React from "react";

jest.mock("react-router-dom", () => ({
  Link: ({ children }: { children: React.ReactNode }): React.ReactNode => (
    <>{children}</>
  ),
  Route: ({ children }: { children: React.ReactNode }): React.ReactNode => (
    <>{children}</>
  ),
  Routes: ({ children }: { children: React.ReactNode }): React.ReactNode => (
    <>{children}</>
  ),
}));

jest.mock(
  "hotel/HotelState",
  () => ({
    actions: { increment: jest.fn() },
    selectors: { getCount: jest.fn() },
  }),
  {
    virtual: true,
  }
);

jest.mock("car/CarApp", (): React.ReactNode => <></>, { virtual: true });
jest.mock("flight/FlightApp", (): React.ReactNode => <></>, { virtual: true });
jest.mock("hotel/HotelApp", (): React.ReactNode => <></>, { virtual: true });
jest.mock("train/TrainApp", (): React.ReactNode => <></>, { virtual: true });
