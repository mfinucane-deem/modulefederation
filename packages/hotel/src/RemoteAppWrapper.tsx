import React, { useEffect } from "react";
import { Provider, useDispatch, useSelector } from "react-redux";

import HotelState from "./state";
import App from "./App";

const RemoteApp = (): React.ReactNode => {
  const dispatch = useDispatch();
  const count: number = useSelector(HotelState.selectors.getCount);

  return (
    <App
      count={count}
      increment={() => dispatch(HotelState.actions.increment())}
    />
  );
};

const RemoteAppWrapper = ({ store }): React.ReactNode => {
  useEffect((): void => {
    store.injectReducer("hotel", HotelState.reducer);
  }, []);

  return (
    <Provider store={store || {}}>
      <RemoteApp />
    </Provider>
  );
};

export default RemoteAppWrapper;
