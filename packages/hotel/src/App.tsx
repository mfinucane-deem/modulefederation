import React, { useCallback, useEffect } from "react";

export interface Props {
  count: number;
  increment: () => void;
}

const App = ({ count, increment }: Props): React.ReactNode => {
  useEffect((): void => {
    console.log(`Current count: ${count}`);
  }, []);

  const onButtonClick = useCallback(increment, []);

  return (
    <div>
      <h1>Hotel: App</h1>
      <button onClick={onButtonClick}>Book a hotel</button>
      <p>You have booked {count} hotel stays.</p>
    </div>
  );
};

export default App;
