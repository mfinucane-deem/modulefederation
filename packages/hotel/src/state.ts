/** Type definitions */

interface Action {
  type: string;
}

interface State {
  count: number;
}

/** Action types */
const reducerName = "HOTEL";
const INCREMENT = `${reducerName}/INCREMENT`;

/** Actions */
const increment = (): Action => ({
  type: INCREMENT,
});

/** State */
const initialState: State = {
  count: 0,
};

/** Reducer */
const reducer = (state: State = initialState, action: Action): State => {
  const { type } = action;

  switch (type) {
    case INCREMENT: {
      return {
        ...state,
        count: state.count + 1,
      };
    }
    default: {
      return state;
    }
  }
};

/** Selectors */
const getCount = ({ hotel }: { hotel: State }): number => hotel?.count;

export default {
  name: reducerName,
  actions: {
    increment,
  },
  types: {
    INCREMENT,
  },
  reducer,
  selectors: {
    getCount,
  },
};
