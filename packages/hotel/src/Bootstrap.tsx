import React, { StrictMode } from "react";
import { createRoot, Root } from "react-dom/client";
import { Store } from "redux";
import { Provider } from "react-redux";

import mainHotelStore from "./store";
import ConnectedApp from "./ConnectedApp";

/**
 * TODO: store when hosted or standalone - sharing
 */
const store: Store = mainHotelStore();
const rootElement: HTMLElement | null = document.getElementById("hotel-app");
const root: Root = createRoot(rootElement!);

root?.render(
  <StrictMode>
    <Provider store={store}>
      <ConnectedApp />
    </Provider>
  </StrictMode>
);
