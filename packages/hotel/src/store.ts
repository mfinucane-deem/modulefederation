import { combineReducers, createStore, Reducer, Store } from "redux";

import HotelState from "./state";

const rootReducer: Reducer = combineReducers({
  hotel: HotelState.reducer,
});

const store = (): Store => createStore(rootReducer);

export default store;
