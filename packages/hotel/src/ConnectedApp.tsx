import { connect } from "react-redux";

import HotelState from "./state";
import App from "./App";

const mapStateToProps = (state: any) => ({
  count: HotelState.selectors.getCount(state),
});

const mapDispatchToProps = (dispatch: any) => ({
  increment: (): void => dispatch(HotelState.actions.increment()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
