import React from "react";
import { render } from "@testing-library/react";

import App, { Props } from "./App";

const defaultProps: Props = {
  count: 0,
  increment: jest.fn(),
};

describe("App tests", (): void => {
  it("renders the component", (): void => {
    expect(() => render(<App {...defaultProps} />)).not.toThrow();
  });
});
