# Module federation example

## What is this?

This is a very basic example of a [micro-frontend](https://martinfowler.com/articles/micro-frontends.html) project using [React](https://react.dev/), [Lerna](https://lerna.js.org/) and [module federation](https://webpack.js.org/concepts/module-federation/). A micro-frontend project is one where the logical sections of the project are separated out into their own mini-projects and kept together using a host project.

In the case of this example, we imagine a web based travel application where users can book hotel, flights and car rentals. Each of these tasks are separated out in to their own applications.

## Basic structure

- The previously mentioned projects (domains) are stored in the `packages/` directory.
- Each subdirectory (car, flight, host, hotel) is self contained with its own dependencies and set up.
- Each of these projects (domains) can be run indepdenty.
- [Lerna](https://lerna.js.org/) is used to run tasks for each of these projects in parallel, including tests.
- All apps (in the `packages/` directory) have their own custom webpack set up apart from `packages/train`, which was initialised with [Create React App](https://create-react-app.dev/).

## Setup and run

- From the project root directory, run `$> yarn` to install dependencies. **Note:** This will install dependencies for all projects in `/pacakges`.
- To start, run `$> yarn dev`. **Note:** This will run `yarn dev` for all projects and boot them in parallel.
- To test, run `$> yarn test`. **Note:** This will run each of the project tests in parallel.

See `package.json` in the root directory for more scripts, where the Lerna scope is used to target individual projects.

## Why?

As applications grow in size, tech debt and maintencance accumulates. The traditional monolithic approach to application development means more complicated and slower releases as features grow in size. It takes longer to run tests, code checks and deployments. Why release an entire app, woith the need to smoke test everything, when parts of the app can be released instead? This is the problem that micro-frontend development aims to solve.

## How?

We use something called [module federation](https://webpack.js.org/concepts/module-federation/) which runs a series of mini-applications through a main host application. In the case of this example, we will run mini applications for flight, car and hotel through our main host application.

Each of these mini applications has their own sets of dependencies, their own entry points and their own webpack configurations. They expose their main entry points (`App.tsx` for now) and the main host application can read these in, importing them in the main host `App.tsx` using [React lazy loading](https://react.dev/reference/react/lazy).

Using [react-router-dom](https://github.com/remix-run/react-router) we navigate between each of these imported views and load the relevant micro applications.

**Note:** The `packages/train` app was initialised using [Create React App](https://create-react-app.dev/) which uses webpack under the hood, but hides its implementation. To augment this hidden webpack configuration with Module Federation in mind, we use a tool called [Craco](https://craco.js.org/).

## Roadmap

- Can we export then re-import things other than components from the child applications so they can be shared universally? It would be nice to be able to share Redux state management for instance to satisfy business logic. Wouldn't it be nice to know about flights so we could suggest hotels for instance?
- Can we create common configurations for each of the child projects, such as test setup, code quality rules, build and deployment configurations to name but a few.

## Some reading

- [Micro frontend resources](https://github.com/billyjov/microfrontend-resources)
- [Module federation webpack docs](https://webpack.js.org/concepts/module-federation/)
- [Module federation examples](https://github.com/module-federation/module-federation-examples/tree/master/redux-reducer-injection)
- [Build Micro frontends in NextJS and ReactJS with Webpack 5 Module Federation](https://medium.com/a-layman/build-micro-frontends-in-nextjs-and-reactjs-with-webpack-5-module-federation-e142ad76f48c)
- [How to create a Micro Frontend application using React](https://indepth.dev/tutorials/react/create-micro-frontend-react)
- [Run tasks individually or in parallel with Lerna](https://lerna.js.org/)
- [Override webpack in CRA with Craco](https://craco.js.org/)
